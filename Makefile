INSTALL := /usr/bin/install
NPM := npm
BIN := ./node_modules/.bin
HEXO := $(BIN)/hexo
HEXO_FLAGS :=

NAME := roanapur
VERSION := $(shell git describe --abbrev=0 --tags 2> /dev/null || echo '1.0.0')

all: node_modules public pack

node_modules: package.json
	$(NPM) prune && $(NPM) install

## optipng: $(patsubst %.png,%.min.png,$(filter-out %.min.png,$(IMAGES_FILES)))

## %.gz: %
## 	gzip -9 -c -f $< > $@

## %.min.png: %.png ## Minify images
## 	optipng --preserve -q -o 2 $< -out $@

themes/hexo-theme-roanapur:
	cp -r node_modules/@terenc3/hexo-theme-roanapur/ $(dir $@)

public: themes/hexo-theme-roanapur source/* ## Generate website
	$(HEXO) $(HEXO_FLAGS) generate

clean: ## Clean public folder and db.json
	$(HEXO) clean
	rm -rf themes/hexo-theme-roanapur

deploy: public/* ## Execute deploy target
	$(HEXO) $(HEXO_FLAGS) deploy

pack: $(NAME)-$(VERSION)-dist.tgz ## Create browser archive
	@echo $(NAME)-$(VERSION)

dist: ## Copy all assets to dist
	$(INSTALL) -d $@

$(NAME)-$(VERSION)-dist.tgz: $(wildcard public/*)
	tar -C public/ -zcvf $@ .

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'

.PHONY: help clean deploy %.min.png
