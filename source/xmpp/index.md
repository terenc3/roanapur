---
title: XMPP
date: 2022-11-03 14:32:03
---

{% blockquote XMPP Standards Foundation https://xmpp.org/%}
The universal messaging standard
{% endblockquote %}

Ich betreibe eine private XMPP Instanz für meine Familie und einige anonyme und öffentliche MUCs.

Erreichen kann man mich entweder direkt über ein XMPP Client ([terenc3@conference.roanapur.de](xmpp:terenc3@conference.roanapur.de?join)) oder per unten stehenden Web Client.

---
{% converse %}

---