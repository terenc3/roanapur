---
title: Impressum
date: 2022-10-27 20:50:31
---

# Angaben gemäß § 5 <abbr title="Telemediengesetz">TMG</abbr>
<address>Benjamin Kahlau<br />
Kapitän-Lehmann-Weg 26<br />
14929 Treuenbrietzen</address>

# Kontakt
E-Mail: info [at] roanapur [dot] de

# Verantwortlich für den Inhalt nach § 55 Abs. 2 <abbr title="Rundfunkstaatsvertrag">RStV</abbr>
<address>Benjamin Kahlau<br />
Kapitän-Lehmann-Weg 26<br />
14929 Treuenbrietzen</address>
