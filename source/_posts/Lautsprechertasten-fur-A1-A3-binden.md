---
title: Lautsprechertasten für A1-A3 binden
date: 2022-11-01 08:33:14
categories:
- Software
tags:
- Voicemeeter
- Macrobuttons
---

Über das Menü von VoiceMeter kann man Lautstärketasten der Tastatur für den Ausgang 1 oder Eingang 1 binden. Wenn man jedoch andere Regler oder mehrere ansprechen will muss man ein Makroknopf erstellen.
<!-- more -->
Wichtig ist hier die Kontrollbox **Exclusive Key** damit nicht zusätzlich die Standardaktion für die Tasten ausgeführt wird.

![Ausgang 1 bis 3|w-1/2](Lautsprechertasten-fur-A1-A3-binden/louder.png)
![Stumm|w-1/2](Lautsprechertasten-fur-A1-A3-binden/mute.png)
