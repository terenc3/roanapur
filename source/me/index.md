---
layout: empty
date: 2022-11-04 10:01:06
---
<main class="bg-neutral-800 text-white w-full h-screen p-8">
    <div class="mx-auto" style="width: 300px;">
        {% img mx-auto /developer-300px.png %}
        <div class="text-3xl mb-4 text-center">Benjamin Kahlau</div>
        <hr class="mb-4"/>
        <div class="text-xl">
            <ul class="mb-4">
                <li class="py-2"><a target="_blank" rel="noopener" href="https://roanapur.de"><img class="w-8 float-left mr-2" src="/developper-300px.png" alt="Blog">Blog</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://troet.cafe/@terenc3"><img class="w-8 float-left mr-2" src="/me/mastodon.svg" alt="Mastodon">Mastodon</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://matrix.to/#/@terenc3:tchncs.de"><img class="w-8 float-left mr-2" src="/me/matrix.svg" alt="Matrix">Matrix</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://reddit.com/u/terenc3/"><img class="w-8 float-left mr-2" src="/me/reddit.svg" alt="Reddit">Reddit</a></li>
            </ul>
            <div class="text-2xl mb-4 text-red-600">Terenc3</div>
            <ul class="mb-4 px-4">
                <li class="py-2"><a target="_blank" rel="noopener" href="https://codeberg.org/terenc3/"><img class="w-8 float-left mr-2" src="/me/codeberg.svg" alt="Codeberg">Codeberg</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://github.com/terenc3/"><img class="w-8 float-left mr-2" src="/me/github.svg" alt="GitHub">GitHub</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://liberapay.com/terenc3/"><img class="w-8 float-left mr-2" src="/me/liberapay.svg" alt="Liberapay">Liberapay</a></li>
            </ul>
            <hr class="mb-4"/>
            <div class="text-2xl mb-4 text-red-600">HerrTerenc3</div>
            <ul>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://tube.tchncs.de/c/herrterenc3/"><img class="w-8 float-left mr-2" src="/me/peertube.svg" alt="PeerTube">PeerTube</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://steamcommunity.com/id/HerrTerenc3/"><img class="w-8 float-left mr-2" src="/me/steam.svg" alt="Steam">Steam</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://www.twitch.tv/HerrTerenc3"><img class="w-8 float-left mr-2" src="/me/twitch.svg" alt="Twitch">Twitch</a></li>
                <li class="py-2"><a target="_blank" rel="noopener" href="https://www.gog.com/u/HerrTerenc3"><img class="w-8 float-left mr-2" src="/me/gogdotcom.svg" alt="GOG.com">GOG.com</a></li>
            </ul>
        </div>
    </div>
</main>