---
title: Datenschutz
date: 2022-10-27 21:15:38
---
# Verantwortlicher
<address>Benjamin Kahlau<br />
Kapitän-Lehmann-Weg 26<br />
14929 Treuenbrietzen</address>

# Rechte
Gegenüber dem genannten Verantwortlichen können Sie jederzeit folgende Rechte ausüben.
- Auskunfsrecht (Art. 15 <abbr title="Datenschutz-Grundverordnung">DSGVO</abbr>)
- Recht auf Berichtigung (Art. 16 <abbr title="Datenschutz-Grundverordnung">DSGVO</abbr>)
- Recht auf Löschung (Art. 17 <abbr title="Datenschutz-Grundverordnung">DSGVO</abbr>)
- Recht auf Einschränkung der Verarbeitung (Art. 18 <abbr title="Datenschutz-Grundverordnung">DSGVO</abbr>)
- Recht auf Datenübertragbarkeit (Art. 20 <abbr title="Datenschutz-Grundverordnung">DSGVO</abbr>)
- Wiederspruchsrecht (Art. 21 <abbr title="Datenschutz-Grundverordnung">DSGVO</abbr>)

# Bereitstellung
Wir protokollieren die Aufrufe unser Webseite zur Sicherung der Vertrauchlichkeit, Verfügbarkeit und Integrität dieser.

Erfasste Daten:

- IP Adresse
- Datum und Uhrzeit
- Angefrage
- Antwortstatus
- Antwortgröße
- Herkunftsseite
- Browserkennung

Diese Daten werden auf Grund berechtigtem Interesse nach Art. 6 Abs. 1 lit. f <abbr title="Datenschutz-Grundverordnung">DSGVO</abbr> verarbeitet und werden für 14 Tage aufgehoben.