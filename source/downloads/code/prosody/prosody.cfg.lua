admins = { "admin@domain.example" }

modules_enabled = {
    "roster"; -- Allow users to have a roster. Recommended ;)
    "saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
    "tls"; -- Add support for secure TLS on c2s/s2s connections
    "dialback"; -- s2s dialback support
    "disco"; -- Service discovery
    "private"; -- Private XML storage (for room bookmarks, etc.)
    "vcard"; -- Allow users to set vCards
    --"privacy"; -- Support privacy lists
    --"compression"; -- Stream compression (Debian: requires lua-zlib module to work)
    "version"; -- Replies to server version requests
    "uptime"; -- Report how long server has been running
    "time"; -- Let others know the time here on this server
    "ping"; -- Replies to XMPP pings with pongs
    "pep"; -- Enables users to publish their mood, activity, playing music and more
    "register"; -- Allow users to register on this server using a client and change passwords

    "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
    "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
    "webpresence"; -- online status image
    "websocket";
    "carbons";
    "mam";
    "announce";
};

modules_disabled = {};

allow_registration = false;

cross_domain_websocket = true;
cross_domain_bosh = true;

-- nginx handle certificates
consider_websocket_secure = true;
consider_bosh_secure = true;

-- http_external_url = "https://roanapur.de";

daemonize = false;

pidfile = "/var/run/prosody/prosody.pid";

c2s_require_encryption = true
s2s_secure_auth = false

authentication = "internal_hashed"

https_ports = { };

log = {
    info = "/var/log/prosody/prosody.log";
    error = "/var/log/prosody/prosody.err";
    { levels = { "error" }; to = "syslog";  };
}

-- plugin_paths = { "/opt/prosody-modules" }

Component "conference.domain.example" "muc"
modules_enabled = {
    "muc_mam",
}

Component "upload.domain.example" "http_file_share"

Include "conf.d/*.cfg.lua"