---
title: Demo
date: 2023-09-22 17:06:02
---

# Überschrift 1
## Überschrift 2
### Überschrift 3
#### Überschrift 4
##### Überschrift 5
###### Überschrift 6

# Listen
- Eins
- Zwei
  - Zwei a
- Drei

# Links
Intern [/hello-world/](/hello-world/)
Extern https://roanapur.de


{% post_link git-tipps-und-tricks %}

# Zitat
{% blockquote The Mumble Developers https://www.mumble.info/about/ %}
Mumble is a free, open source, low latency, high quality voice chat application.
{% endblockquote %}

{% blockquote @DevDocs https://twitter.com/devdocs/status/356095192085962752 %}
NEW: DevDocs now comes with syntax highlighting. http://devdocs.io
{% endblockquote %}

# Trenner
---

# Code
{% code hello-world.js lang:js %}
console.log("Hello World");
{% endcode %}

# Markdown
## abbr
HTML

*[HTML]: Hyper Text Markup Language

{% code %}
HTML

*[HTML]: Hyper Text Markup Language
{% endcode %}

# Emoji
> node_modules/markdown-it-emoji/lib/data/full.json

:heart:
:white_check_mark:

{% code %}
:heart:
:white_check_mark:
{% endcode %}

# Spoiler
<details>
    <summary>Spoiler!</summary>
    Doch nicht!
</details>

# KBD / SAMP
Please press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>R</kbd> to re-render an MDN page.

You can use <abbr title="Cascading Style Sheets">CSS</abbr> to style your <abbr>HTML</abbr> (HyperText Markup Language).

<kbd><kbd>Ctrl</kbd>+<kbd>N</kbd></kbd>

<samp><kbd>git add my-new-file.cpp</kbd></samp>
<code>
    <samp><kbd>custom-git ad my-new-file.cpp</kbd></samp>
</code>

To create a new file, choose the <kbd><kbd><samp>File</samp></kbd><kbd><samp>New Document</samp></kbd></kbd> menu option.

<pre><code><samp><kbd>md5 -s "Hello world"</kbd>
MD5 ("Hello world") = 3e25960a79dbc69b674cd4ec67a72c62
<kbd>md5 -s "Hello world"</kbd>
MD5 ("Hello world") = 3e25960a79dbc69b674cd4ec67a72c62
</samp></pre></code>

# Fußzeilen
Siehe [^vscode_git]

[^vscode_git]: [Resolving Git line ending issues in WSL](https://code.visualstudio.com/docs/remote/troubleshooting#_github-codespaces-tips)

{% code %}
Siehe [^vscode_git]

[^vscode_git]: [Resolving Git line ending issues in WSL](https://code.visualstudio.com/docs
{% endcode %}
