---
title: Mumble
date: 2022-11-02 09:58:31
---

{% blockquote The Mumble Developers https://www.mumble.info/about/  mumble.info/about/%}
Mumble is a free, open source, low latency, high quality voice chat application.
{% endblockquote %}

Ich betreibe eine Mumble Instanz die gelegentlich zum Plauder oder als Teamkanal für Spiele genutzt wird. Jeder darf den Server betreten aber nur authentifizierte Nutzer dürfen andere Kanäle als die Lobby betreten.

Betreten kann diese entweder direkt über Mumble ([mumble://voice.roanapur.de/](mumble://voice.roanapur.de/?version=1.2.0&title=Roanapur&url=https://voice.roanapur.de)) oder per Mumble Web (https://voice.roanapur.de/)

---
<iframe src="https://voice.roanapur.de/viewer/" style="min-height: 480px;
width: 100%;" class="border-2"></iframe>

---
