---
title: Hello World
date: 2022-10-30 12:23:27
---
{% img w-1/3 ml-4 float-right /hello-world/benjamin_kahlau.jpg %}

Ich bin Benjamin Kahlau ‒ ein __Software Entwickler__ der willens ist jede neue Technologie und jedes Framework zu testen um es anschließend zu verwerfen.

## Platformen
<p class="flex flex-wrap">
    <i class="devicon-linux-plain colored text-6xl mr-2" title="Linux" role="img"></i>
    <i class="devicon-debian-plain colored text-6xl mr-2" title="Debian" role="img"></i>
    <i class="devicon-raspberrypi-line colored text-6xl mr-2" title="RaspBerryPi" role="img"></i>
    <i class="devicon-android-plain colored text-6xl mr-2" title="Android" role="img"></i>
    <i class="devicon-windows8-original colored text-6xl mr-2" title="Windows" role="img"></i>
</p>

## Sprachen
<p class="flex flex-wrap">
    <i class="devicon-html5-plain colored text-6xl mr-2" title="HTML" role="img"></i>
    <i class="devicon-css3-plain colored text-6xl mr-2" title="CSS" role="img"></i>
    <i class="devicon-sass-original colored text-6xl mr-2" title="Sass" role="img"></i>
    <i class="devicon-javascript-plain colored text-6xl mr-2" title="JavaScript" role="img"></i>
    <i class="devicon-java-plain colored text-6xl mr-2" title="Java" role="img"></i>
    <i class="devicon-kotlin-plain colored text-6xl mr-2" title="Kotlin" role="img"></i>
    <i class="devicon-nodejs-plain colored text-6xl mr-2" title="Node.js" role="img"></i>
    <i class="devicon-php-plain colored text-6xl mr-2" title="PHP" role="img"></i>
    <i class="devicon-ruby-plain colored text-6xl mr-2" title="Ruby" role="img"></i>
    <i class="devicon-python-plain colored text-6xl mr-2" title="Python" role="img"></i>
    <i class="devicon-latex-original colored text-6xl mr-2" title="LaTeX" role="img"></i>
</p>

## Frameworks
<p class="flex flex-wrap">
    <i class="devicon-angularjs-plain colored text-6xl mr-2" title="AngularJS" role="img"></i>
    <i class="devicon-vuejs-plain colored text-6xl mr-2" title="Vue.js" role="img"></i>
    <i class="devicon-jquery-plain-wordmark colored text-6xl mr-2" title="jQuery" role="img"></i>
    <i class="devicon-tailwindcss-plain colored text-6xl mr-2" title="Tailwind CSS" role="img"></i>
    <i class="devicon-bootstrap-plain colored text-6xl mr-2" title="Bootstrap"></i>
    <i class="devicon-cakephp-plain colored text-6xl mr-2" title="CakePHP" role="img"></i>
    <i class="devicon-d3js-plain colored text-6xl mr-2" title="D3.js" role="img"></i>
</p>

## Testing
<p class="flex flex-wrap">
    <i class="devicon-jasmine-plain colored text-6xl mr-2" title="Jasmine" role="img"></i>
    <i class="devicon-karma-plain colored text-6xl mr-2" title="Karma" role="img"></i>
    <i class="devicon-selenium-original colored text-6xl mr-2" title="Selenium" role="img"></i>
</p>


## Tools
<p class="flex flex-wrap">
    <i class="devicon-git-plain colored text-6xl mr-2" title="Git" role="img"></i>
    <i class="devicon-npm-original-wordmark colored text-6xl mr-2" title="npm" role="img"></i>
    <i class="devicon-grunt-plain colored text-6xl mr-2" title="Grunt" role="img"></i>
    <i class="devicon-bower-plain colored text-6xl mr-2" title="Bower" role="img"></i>
    <i class="devicon-webpack-plain colored text-6xl mr-2" title="webpack" role="img"></i>
    <i class="devicon-vagrant-plain colored text-6xl mr-2" title="Vagrant"></i>
    <i class="devicon-babel-plain colored text-6xl mr-2" title="Babel" role="img"></i>
    <i class="devicon-gradle-plain colored text-6xl mr-2" title="Gradle" role="img"></i>
</p>

## Software
<p class="flex flex-wrap">
    <i class="devicon-wordpress-plain colored text-6xl mr-2" title="Wordpress" role="img"></i>
    <i class="devicon-mysql-plain colored text-6xl mr-2" title="MySql" role="img"></i>
    <i class="devicon-nginx-original colored text-6xl mr-2" title="NGINX" role="img"></i>
    <i class="devicon-phpstorm-plain colored text-6xl mr-2" title="PhpStorm" role="img"></i>
</p>

## DevOps
<p class="flex flex-wrap">
    <i class="devicon-jenkins-line text-6xl mr-2" title="Jenkins" role="img"></i>
    <i class="devicon-ansible-plain colored text-6xl mr-2" title="Ansible" role="img"></i>
</p>