const { encodeURL, url_for } = require('hexo-util')

function resolve(from, to) {
    const resolvedUrl = new URL(to, new URL(from, 'resolve://'));
    if (resolvedUrl.protocol === 'resolve:') {
        // `from` is a relative URL.
        const { pathname, search, hash } = resolvedUrl;
        return pathname + search + hash;
    }
    return resolvedUrl.toString();
}

/*
hexo.extend.tag.register('js', function(args, content) {
    return 'script src="' + args[0] + '"></script';
});
*/

/**
 * Convert markdown images makros to figure tags
 *
 * ![example|w-1/2](postname/example.jpg)  -->  <figure class="w-1/2"><img src="postname/example.jpg" /><figcaption>example</figcaption></figure> %}
 *
 * @see {@link https://github.com/yiyungent/hexo-asset-img}
 */
hexo.extend.filter.register('before_post_render', (data) => {
    const reverseSource = data.source.split('').reverse().join('')
    const fileName = reverseSource.substring(3, reverseSource.indexOf('/')).split('').reverse().join('')

    const PostAsset = hexo.model('PostAsset')
    data.content = data.content.replace(RegExp('!\\[(.*?)\\]\\(' + fileName + '/(.+?)\\)', 'g'), (match, title, file) => {
        const asset = PostAsset.findOne({
            post: data._id,
            slug: file
        })

        const url = url_for.call(hexo, encodeURL(resolve('/', asset.path)))

        const parts = title.split('|')
        return '<figure class="image ' + parts[1] + '"><a href="' + url + '"><img src="' + url + '" title="' + parts[0] + '" alt="' + parts[0] + '" /></a><figcaption>' + parts[0] + '</figcaption></figure>'
    })
    return data
}, 0);
